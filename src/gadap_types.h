/* Copyright (C) 2003-2008 by the 
 * Institute of Global Environment and Society (IGES)
 * See the file COPYRIGHT for more information.   
 *
 *
 * gadods_types.h - Data type and constant definitions for the gadap library.
 *
 */

#ifndef _gadap_types_h_
#define _gadap_types_h_

/** Sets the maximum number of datasets that can be open simultaneously.
 * Can be set to any integer. */
#define GADAP_MAX_DATASETS 255

/* Return type indicating either success or an error. */
typedef int GADAP_STATUS;

/* Handle type for an open dataset. Valid handles are always non-negative. */
typedef int GADAP_DATASET;

/* Handle type for a collection of station data reports */
typedef int GADAP_RPTCOL;

/* The gadap library treats data as two basic types: text or
 * numeric. Different DAP numeric encodings are all converted to
 * double precision floating point. String or URL data are
 * considered text. Complex types like Structures, Sequences, Grids
 * and Arrays are handled internally and not presented as
 * variables. */
typedef enum gadap_var_types_enum {
  GADAP_INVALID_TYPE = 0,
  GADAP_DBL_TYPE, /* text data */
  GADAP_STR_TYPE  /* numeric data */
} GADAP_VAR_TYPE;

/* Structure containing query parameters for requesting station data */
typedef struct {
  /* Handle for the dataset associated with this query. */
  GADAP_DATASET d_handle;

  /* Array of flags indicating whether each variable should be included in
   * the query. The array must have at least as many elements as there
   * are variables in the dataset to be queried. For each variable of the
   * dataset, the corresponding entry in varflags should be set to 0 to 
   * exclude the variable from the query, or a non-zero value to include it. 
   * If varflags is NULL, all variables will be requested. If varflags is
   * not NULL, then at least one entry must be non-zero for the query to
   * be sent. */
  int *varflags;
  
  /* Flag indicating whether the query should be restricted to particular
   * ranges of space and time. If zero, the values in minlon, maxlon, etc. 
   * will be ignored. If non-zero they will be included in the query. In this 
   * case all bounds must be set to meaningful values, including mintime and
   * maxtime.
   */
  int usebounds;

  /* Minimum longitude for the query */
  float minlon;
  /* Maximum longitude for the query */
  float maxlon;

  /* Minimum latitude for the query */
  float minlat;
  /* Maximum latitude for the query */
  float maxlat;

  /* Minimum vertical level for the query */
  float minlev;
  /* Maximum vertical level for the query */
  float maxlev;

  /* Minimum time for the query */
  const char *mintime;
  /* Maximum time for the query */
  const char *maxtime;

  /* If not NULL, restricts the query to the specific station ID given. 
   * Note that currently station ID and geographic bounds cannot be 
   * specified simultaneously. 
   */
  const char *stid;

  /* If not NULL, specifies additional DAP constraints to apply to 
   * the query. Each constraint must be preceded by "&". 
   * Example: "&var1<0" would restrict the query to reports where the 
   * variable var1 is negative. */
  const char *extra;

  /* The completed URL is stored here.
   * This string should not be modified. It is
   * freed by the gadap_sq_free() function. */
  char *url;

  /* This points to the constraint portion of the URL buffer.
   * This should not be modified. It is
   * freed by the gadap_sq_free() function. */
  char *constraint;

} GADAP_STN_QUERY;

#endif /* _gadap_types_h_ */
