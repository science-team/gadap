/* Copyright (C) 2003-2008 by the 
 * Institute of Global Environment and Society (IGES)
 * See the file COPYRIGHT for more information.   
 *
 *
 * gadap_err.h: Error codes for the gadap library.
 *
 * Note that all actual returned errors are *negative*. 
 * E.g. to test for a DAP error, the return code should be 
 * compared to errval(GADAP_ERR_DAP_ERR), not just GADAP_ERR_DAP_ERR.
 * 
 */

#ifndef _gadap_err_h_
#define _gadap_err_h_

#define errval(val) (-1 * val)

enum gadap_err_codes {
  GADAP_SUCCESS = 0,
  GADAP_ERR_INVALID_HANDLE, 
  GADAP_ERR_INDEX_OUT_OF_RANGE,
  GADAP_ERR_NAME_NOT_FOUND,
  GADAP_ERR_INVALID_QUERY,
  GADAP_ERR_DAP_ERROR,
  GADAP_ERR_BAD_NUMERIC_FORMAT, 
  GADAP_ERR_BAD_VARIABLE_TYPE,
  GADAP_ERR_UNKNOWN_LENGTH
};

#endif
