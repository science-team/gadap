
// -*- mode: c++; c-basic-offset:4 -*-

// This file is part of libdap, A C++ implementation of the OPeNDAP Data
// Access Protocol.

// Copyright (c) 2002,2003 OPeNDAP, Inc.
// Author: James Gallagher <jgallagher@opendap.org>
//         Reza Nekovei <reza@intcomm.net>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// You can contact OPeNDAP, Inc. at PO Box 112, Saunderstown, RI. 02874-0112.
 
// (c) COPYRIGHT URI/MIT 1994-1998
// Please read the full copyright statement in the file COPYRIGHT_URI.
//
// Authors:
//      jhrg,jimg       James Gallagher <jgallagher@gso.uri.edu>
//      reza            Reza Nekovei <reza@intcomm.net>

// This file contains the interface definition for the class Connections,
// which manages a set of Connect objects. Once created, a Connect * can be
// added (managed) and refferred to by an integer. The int can also be used
// to delete the Connect *.
//
// This class should become a mapping between (Connect *)s and some other
// type -- the type normally used by an API to refer to one of its
// objects/files. For netcdf and JGOFS, int will do just fine, but other APIs
// (HDF ?) use pointers. 
//
// jhrg 9/30/94

#ifndef _connections_h
#define _connections_h

#include <vector>

const int MAX_CONNECTIONS = 64;

/** @brief Holds an ensemble of Connect objects.

    This class is designed to hold a collection of Connect objects.
    It contains methods to add and delete Connect objects from the collection,
    and defines the index operator ([]) to retrieve individual objects.

    @see Connect, NCConnect */
template <class T>
class Connections {
private:
    int _max_con;
    std::vector<int> _free;		// free list of connections
    T *_conns;			// array alloc'd at object construction

    void init_array(int i);

public:
    Connections(int max_connections = MAX_CONNECTIONS);

    Connections(const Connections<T> &cs);
    virtual ~Connections();

    Connections &operator=(const Connections<T> &rhs);

    T &operator[](int i);

    int add_connect(T c);

    void del_connect(int i);
    
    int free_elements();
};

/* 
 * $Log: Connections.h,v $
 * Revision 1.1  2008/07/25 20:43:25  jma
 * Overhaul of code to be compatible with libdap 3.7.
 * All references of "dods" changed to "dap" including the name of the library.
 * Changed to GPL license. New configure scripts.
 *
 * Revision 1.1  2005/03/05 00:19:08  jimg
 * Added
 *
 * Revision 1.15  2004/07/07 21:08:47  jimg
 * Merged with release-3-4-8FCS
 *
 * Revision 1.13.2.1  2004/07/02 20:41:51  jimg
 * Removed (commented) the pragma interface/implementation lines. See
 * the ChangeLog for more details. This fixes a build problem on HP/UX.
 *
 * Revision 1.14  2003/12/08 18:02:29  edavis
 * Merge release-3-4 into trunk
 *
 * Revision 1.13  2003/04/22 19:40:27  jimg
 * Merged with 3.3.1.
 *
 * Revision 1.12  2003/02/21 00:14:24  jimg
 * Repaired copyright.
 *
 * Revision 1.11.2.1  2003/02/21 00:10:07  jimg
 * Repaired copyright.
 *
 * Revision 1.11  2003/01/23 00:22:24  jimg
 * Updated the copyright notice; this implementation of the DAP is
 * copyrighted by OPeNDAP, Inc.
 *
 * Revision 1.10  2003/01/10 19:46:40  jimg
 * Merged with code tagged release-3-2-10 on the release-3-2 branch. In many
 * cases files were added on that branch (so they appear on the trunk for
 * the first time).
 *
 * Revision 1.8.4.2  2002/09/13 16:10:45  jimg
 * Added std::vector;
 *
 * Revision 1.8.4.1  2002/09/05 22:52:54  pwest
 * Replaced the GNU data structures SLList and DLList with the STL container
 * class vector<>. To maintain use of Pix, changed the Pix.h header file to
 * redefine Pix to be an IteratorAdapter. Usage remains the same and all code
 * outside of the DAP should compile and link with no problems. Added methods
 * to the different classes where Pix is used to include methods to use STL
 * iterators. Replaced the use of Pix within the DAP to use iterators instead.
 * Updated comments for documentation, updated the test suites, and added some
 * unit tests. Updated the Makefile to remove GNU/SLList and GNU/DLList.
 *
 * Revision 1.9  2002/06/18 15:36:24  tom
 * Moved comments and edited to accommodate doxygen documentation-generator.
 *
 * Revision 1.8  2000/09/22 02:17:19  jimg
 * Rearranged source files so that the CVS logs appear at the end rather than
 * the start. Also made the ifdef guard symbols use the same naming scheme and
 * wrapped headers included in other headers in those guard symbols (to cut
 * down on extraneous file processing - See Lakos).
 *
 * Revision 1.7  2000/08/02 22:46:48  jimg
 * Merged 3.1.8
 *
 * Revision 1.6.6.2  2000/08/02 21:10:07  jimg
 * Removed the header config_dap.h. If this file uses the dap typedefs for
 * cardinal datatypes, then it gets those definitions from the header
 * dods-datatypes.h.
 *
 * Revision 1.6.6.1  2000/08/01 21:09:35  jimg
 * Destructor is now virtual
 *
 * Revision 1.6  1999/05/04 19:47:20  jimg
 * Fixed copyright statements. Removed more of the GNU classes.
 *
 * Revision 1.5  1998/02/04 14:55:31  tom
 * Another draft of documentation.
 *
 * Revision 1.4  1997/08/11 18:19:13  jimg
 * Fixed comment leaders for new CVS version
 *
 * Revision 1.3  1996/06/08 00:19:46  jimg
 * Added config_dap.h in place of config_netio.h
 *
 * Revision 1.2  1996/05/31 23:29:33  jimg
 * Updated copyright notice.
 *
 * Revision 1.1  1995/01/10 16:23:06  jimg
 * Created new `common code' library for the net I/O stuff.
 *
 * Revision 1.3  1994/11/18  21:12:20  reza
 * Changed it to a template class for derived classes of the Connect
 *
 * Revision 1.2  1994/10/05  20:23:29  jimg
 * Fixed errors in *.h files comments - CVS bites again.
 * Changed request_{das,dds} so that they use the field `_api_name'
 * instead of requiring callers to pass the api name.
 *
 * Revision 1.1  1994/10/05  18:02:11  jimg
 * First version of the connection management classes.
 * This commit also includes early versions of the test code.
 */

#endif // _connections_h
