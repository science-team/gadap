/* Copyright (C) 2003-2008 by the 
 * Institute of Global Environment and Society (IGES)
 * See the file COPYRIGHT for more information.   
 *
 *
 * gaBaseTypes.cc  - Implementations and factory methods for the OPeNDAP basetypes
 *
 * Authored by Joe wielgosz 
 *
 * Last modified: $Date: 2008/07/25 20:43:25 $ 
 * Revision for this file: $Revision: 1.3 $
 * Release name: $Name: gadods-2_0 $
 * Original for this file: $Source: /homes/cvsroot/gadods/gadods/src/gaBaseTypes.cc,v $
 */

#include <Array.h>
#include <Byte.h>
#include <Float32.h>
#include <Float64.h>
#include <Grid.h>
#include <Int16.h>
#include <Int32.h>
#include <Sequence.h>
#include <Str.h>
#include <Structure.h>
#include <UInt16.h>
#include <UInt32.h>
#include <Url.h>
#include <debug.h>

#include "gaBaseTypes.h"
#include "gaTypeFactory.h"

/** class declarations for gaStr and gaUrl are in gaBaseTypes.h so we
 * can't declare them again, we have to define each function
 * individually
 */

/***************** class gaStr ********************/

gaStr::gaStr(const string &n)  : Str(n), publicval(NULL){

  //  cout << "publicval is " << publicval << endl << flush;
}

gaStr::~gaStr() {
  if (publicval) delete publicval;
}

BaseType *gaStr::ptr_duplicate() {
  return new gaStr(*this); 
} 
bool gaStr::read(const string &dataset) {
  cout << "call to read() - not implemented.\n";
}

string * gaStr::getStringVal() {
  if (!publicval) {
    buf2val((void **)(&publicval));
  }
  return publicval;
}

/***************** class gaUrl ********************/

gaUrl::gaUrl(const string &n)  : Url(n), publicval(NULL){
  //  publicval = NULL;
}

gaUrl::~gaUrl() { 
  if (publicval) delete publicval;
}

BaseType *gaUrl::ptr_duplicate() {
  return new gaUrl(*this); 
} 

bool gaUrl::read(const string &dataset) {
  cout << "call to read() - not implemented.\n";
}

string * gaUrl::getStringVal() {
  if (!publicval) {
    buf2val((void **)(&publicval));
  }
  return publicval;
}

/** none of the remainder need to be declared in the header, since
 *  they're not referenced directly in other source files. so we just
 *  declare and define them here.
 */

class gaArray: public Array {
public:
  gaArray(const string &n = "", BaseType *v = 0)  : Array(n, v) {}
  virtual ~gaArray() {}
  virtual BaseType *ptr_duplicate() {
    return new gaArray(*this); 
  } 
  virtual bool read(const string &dataset) {
    cout << "call to read() - not implemented.\n";
  }
};


class gaByte: public Byte {
public:
  gaByte(const string &n = "")  : Byte(n) {}
  virtual ~gaByte() {}
  virtual BaseType *ptr_duplicate() {
    return new gaByte(*this); 
  } 
  virtual bool read(const string &dataset) {
    cout << "call to read() - not implemented.\n";
  }
};


class gaFloat32: public Float32 {
public:
  gaFloat32(const string &n = "")  : Float32(n) {}
  virtual ~gaFloat32() {}
  virtual BaseType *ptr_duplicate() {
    return new gaFloat32(*this); 
  } 
  virtual bool read(const string &dataset) {
    cout << "call to read() - not implemented.\n";
  }
};


class gaFloat64: public Float64 {
public:
  gaFloat64(const string &n = "")  : Float64(n) {}
  virtual ~gaFloat64() {}
  virtual BaseType *ptr_duplicate() {
    return new gaFloat64(*this); 
  } 
  virtual bool read(const string &dataset) {
    cout << "call to read() - not implemented.\n";
  }
};


class gaGrid: public Grid {
public:
  gaGrid(const string &n = "")  : Grid(n) {}
  virtual ~gaGrid() {}
  virtual BaseType *ptr_duplicate() {
    return new gaGrid(*this); 
  } 
  virtual bool read(const string &dataset) {
    cout << "call to read() - not implemented.\n";
  }
};


class gaInt16: public Int16 {
public:
  gaInt16(const string &n = "")  : Int16(n) {}
  virtual ~gaInt16() {}
  virtual BaseType *ptr_duplicate() {
    return new gaInt16(*this); 
  } 
  virtual bool read(const string &dataset) {
    cout << "call to read() - not implemented.\n";
  }
};


class gaInt32: public Int32 {
public:
  gaInt32(const string &n = "")  : Int32(n) {}
  virtual ~gaInt32() {}
  virtual BaseType *ptr_duplicate() {
    return new gaInt32(*this); 
  } 
  virtual bool read(const string &dataset) {
    cout << "call to read() - not implemented.\n";
  }
};

#if 0
class gaList: public List {
public:
  gaList(const string &n = "", BaseType *v = 0)  : List(n, v) {}
  virtual ~gaList() {}
  virtual BaseType *ptr_duplicate() {
    return new gaList(*this); 
  } 
  virtual bool read(const string &dataset) {
    cout << "call to read() - not implemented.\n";
  }
};
#endif

class gaSequence: public Sequence {
public:
  gaSequence(const string &n = "")  : Sequence(n) {}
  virtual ~gaSequence() {}
  virtual BaseType *ptr_duplicate() {
    return new gaSequence(*this); 
  } 
  virtual bool read(const string &dataset) {
    cout << "call to read() - not implemented.\n";
  }
};


class gaStructure: public Structure {
public:
  gaStructure(const string &n = "")  : Structure(n) {}
  virtual ~gaStructure() {}
  virtual BaseType *ptr_duplicate() {
    return new gaStructure(*this); 
  } 
  virtual bool read(const string &dataset) {
    cout << "call to read() - not implemented.\n";
  }
};


class gaUInt16: public UInt16 {

public:
  gaUInt16(const string &n = "")  : UInt16(n) {}
  virtual ~gaUInt16() {}
  virtual BaseType *ptr_duplicate() {
    return new gaUInt16(*this); 
  } 
  virtual bool read(const string &dataset) {
    cout << "call to read() - not implemented.\n";
  }
};


class gaUInt32: public UInt32 {
public:
  gaUInt32(const string &n = "")  : UInt32(n) {}
  virtual ~gaUInt32() {}
  virtual BaseType *ptr_duplicate() {
    return new gaUInt32(*this); 
  } 
  virtual bool read(const string &dataset) {
    cout << "call to read() - not implemented.\n";
  }
};

Byte *
gaTypeFactory::NewByte(const string &n ) const 
{ 
    return new gaByte(n);
}

Int16 *
gaTypeFactory::NewInt16(const string &n ) const 
{ 
    return new gaInt16(n); 
}

UInt16 *
gaTypeFactory::NewUInt16(const string &n ) const 
{ 
    return new gaUInt16(n);
}

Int32 *
gaTypeFactory::NewInt32(const string &n ) const 
{ 
    DBG(cerr << "Inside gaTypeFactory::NewInt32" << endl);
    return new gaInt32(n);
}

UInt32 *
gaTypeFactory::NewUInt32(const string &n ) const 
{ 
    return new gaUInt32(n);
}

Float32 *
gaTypeFactory::NewFloat32(const string &n ) const 
{ 
    return new gaFloat32(n);
}

Float64 *
gaTypeFactory::NewFloat64(const string &n ) const 
{ 
    return new gaFloat64(n);
}

Str *
gaTypeFactory::NewStr(const string &n ) const 
{ 
    return new gaStr(n);
}

Url *
gaTypeFactory::NewUrl(const string &n ) const 
{ 
    return new gaUrl(n);
}

Array *
gaTypeFactory::NewArray(const string &n , BaseType *v) const 
{ 
    return new gaArray(n, v);
}

Structure *
gaTypeFactory::NewStructure(const string &n ) const 
{ 
    return new gaStructure(n);
}

Sequence *
gaTypeFactory::NewSequence(const string &n ) const 
{ 
    DBG(cerr << "Inside gaTypeFactory::NewSequence" << endl);
    return new gaSequence(n);
}

Grid *
gaTypeFactory::NewGrid(const string &n ) const 
{ 
    return new gaGrid(n);
}

#if 0

Array * NewArray(const string &n, BaseType *v) {
  return new gaArray(n, v);
}

Byte * NewByte(const string &n) {
  return new gaByte(n);
}

Float32 * NewFloat32(const string &n) {
  return new gaFloat32(n);
}

Float64 * NewFloat64(const string &n) {
  return new gaFloat64(n);
}

Grid * NewGrid(const string &n) {
  return new gaGrid(n);
}

Int16 * NewInt16(const string &n) {
  return new gaInt16(n);
}

Int32 * NewInt32(const string &n) {
  return new gaInt32(n);
}

#if 0
List * NewList(const string &n, BaseType *v) {
  return new gaList(n, v);
}
#endif

Sequence * NewSequence(const string &n) {
  return new gaSequence(n);
}

Str * NewStr(const string &n) {
  return new gaStr(n);
}

Structure * NewStructure(const string &n) {
  return new gaStructure(n);
}

UInt16 * NewUInt16(const string &n) {
  return new gaUInt16(n);
}

UInt32 * NewUInt32(const string &n) {
  return new gaUInt32(n);
}

Url * NewUrl(const string &n) {
  return new gaUrl(n);
}
#endif
